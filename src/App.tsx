import {Outlet, useLocation, useNavigate} from "react-router-dom";
import {useEffect} from "react";
import {getCookie} from "@/utils/cookieTool";
import {setTitle} from "@/utils/setTitle";

const App = () => {
    const navigate = useNavigate()
    const location = useLocation()

    useEffect(() => {
        if (location.pathname == '/login') {
            document.title = '登录'
        } else if (location.pathname == '/404') {
            document.title = '404'
        } else {
            setTitle(location.pathname)
        }

        if (!(location.pathname == '/login')) {
            if (!getCookie('adminToken')) {
                navigate('/login', {replace: true})
            }
        }
    }, [location.pathname])
    return (<Outlet/>)
}
export default App
