import {configureStore} from '@reduxjs/toolkit'
import loginRelatedSlice from './modules/loginRelated'

export default configureStore({
    reducer: {
        loginRelated: loginRelatedSlice
    }
})
