import {createSlice, Draft, PayloadAction} from '@reduxjs/toolkit'

interface LoginState {
    userName: string | null
}

const initialState: LoginState = {
    userName: '' || localStorage.getItem('account')
}
export const loginRelatedSlice = createSlice({
    name: 'loginRelated',
    initialState,
    reducers: {
        setUserName: (state: Draft<LoginState>, action: any) => {
            state.userName = action.payload
            console.log('添加', state, action)
        },
    }
})
// 每个 case reducer 函数会生成对应的 Action creators
export const {setUserName} = loginRelatedSlice.actions

export default loginRelatedSlice.reducer
