import style from "./index.module.scss"
import VirtualTable from "@/components/virtualTable";

const VirTable = () => {


    const columns = [
        {
            title: 'A', dataIndex: 'id',
            //    width: 200
        },
        {title: 'B', dataIndex: 'id',},
        {title: 'C', dataIndex: 'id',},
        {
            title: 'D', dataIndex: 'id', //    width: 200
        },
        {title: 'E', dataIndex: 'id',},
        {title: 'F', dataIndex: 'id',},
    ];

    const data = Array.from({length: 100000}, (_, id) => ({id}));
    return (<VirtualTable columns={columns} dataSource={data} scroll={{y: 750, x: '100vw'}}/>)
}
export default VirTable
