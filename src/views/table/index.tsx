import style from "./index.module.scss"
import YTable from "@/components/YTable";
import {Space, Tag} from "antd";
import {useImmer} from "use-immer";
import {useEffect, useState} from "react";
import TableButton from "@/components/YTable/component/tableButton";
import Formsearch from "@/components/formsearch";
import {findSearch} from '@/api/home'

const Table = () => {
    const [tableLoading, setTableLoading] = useState<boolean>(false)
    const [pagination, setPagination] = useImmer({
        pageSize: 10, //分页查询的条数
        current: 1, //当前页
        total: 200
    })
    const [selectIds, setSelectIds] = useImmer({
        value: [1, 5]
    })
    const selectChange = ({selectedRowKeys, selectedRows, change}: any) => {
        const arr = [...selectIds.value]
        selectedRowKeys.map((item: any) => {
            arr.push(parseInt(item) as any)
        })
        const newArr = [...new Set(arr)]
        if (!!change) {
            change.remove.map((item: any) => {
                let index = newArr.indexOf(item)
                newArr.splice(index, 1)
            })
        }
        setSelectIds(draft => {
            draft.value = newArr
        });
        //  console.log(newArr)
    }
    const changeSize = ({pagination}: any) => {
        setTableLoading(true)
        setPagination(draft => {
            draft.current = pagination.current
            draft.pageSize = pagination.pageSize
        })
        setTimeout(() => {
            setTableLoading(false)
        }, 800)
        console.log('分页', pagination)
    }
    const getCheckbox = (e: any) => {
        return {
            disabled: e.level == '1',
            name: e.people,
        }
    }

    const columns: any = [
        {
            align: 'center',
            title: '排查描述',
            dataIndex: 'description',
            key: 'description',
            ellipsis: true,
            render: (text: any) => <a>{text}</a>,
        },
        {
            align: 'center',
            title: '隐患级别',
            ellipsis: true,
            dataIndex: 'level',
            key: 'level',
        },
        {
            align: 'center',
            title: '排查性质',
            ellipsis: true,
            dataIndex: 'nature',
            key: 'nature',
        },
        {
            align: 'center',
            title: '流程状态',
            key: 'status',
            dataIndex: 'status',
            render: (_: any, {status}: any) => (
                <>
                    {status.map((tag: string) => {
                        let color = tag.length > 5 ? 'geekblue' : 'green';
                        if (tag === 'loser') {
                            color = 'volcano';
                        }
                        return (
                            <Tag color={color} key={tag}>
                                {tag.toUpperCase()}
                            </Tag>
                        );
                    })}
                </>
            ),
        },
        {
            align: 'center',
            title: '排查人',
            ellipsis: true,
            dataIndex: 'people',
            key: 'people',
        },
        {
            align: 'center',
            title: '排查时间',
            dataIndex: 'time',
            ellipsis: true,
            key: 'time',
        },
        {
            align: 'center',
            title: '操作',
            key: 'operate',
            render: (text: any, record: any, index: any) => (
                <Space>
                    <TableButton click={() => confirm({record,})} options={{
                        disabledFunc: () => record.level == 1,
                        hide: () => record.level == 2
                    }}>确定</TableButton>
                    <TableButton click={() => delBtn({record, index})} options={{danger: true}}>删除</TableButton>
                </Space>
            ),
        },
    ]
    const confirm = (e: any) => {
        console.log('确认', e)
    }
    const delBtn = (e: any) => {
        console.log('删除', e)
    }
    const clickSearch = (e: any) => {
        findSearch({ep_id: 469110}).then((res: any) => {
            console.log('请求成功', res)
        })
        console.log('表单搜索', e)
    }
    const data = [
        {
            id: 1,
            description: '排查描述1',
            level: 0,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: ['状态1', '状态2'],
            //可以展开下一列的数据
            /* children:[
                 {
                     id: 15,
                     description: '排查描述15',
                     level: 0,
                     nature: '排查性质3',
                     people: '排查人224',
                     time: '202t2',
                     status: ['状态1', '状态2'],
                 }
             ]*/
        },
        {
            id: 2,
            description: '排查描述1',
            level: 0,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: ['状态1', '状态2'],
        },
        {
            id: 3,
            description: '排查描述1',
            level: 2,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: ['状态1', '状态2'],
        },
        {
            id: 4,
            description: '排查描述1',
            level: 0,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: ['状态1', '状态2'],
        },
        {
            id: 5,
            description: '排查描述1',
            level: 0,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: ['状态1', '状态2'],
        },
        {
            id: 6,
            description: '排查描述1',
            level: 1,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: ['状态1', '状态2'],
        },
        {
            id: 7,
            description: '排查描述1',
            level: 0,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: ['状态1', '状态2'],
        },
        {
            id: 8,
            description: '排查描述1',
            level: 0,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: ['状态1', '状态2'],
        },
        {
            id: 9,
            description: '排查描述1',
            level: 0,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: ['状态1', '状态2'],
        },
        {
            id: 10,
            description: '排查描述1',
            level: 0,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: ['状态1', '状态2'],
        },
    ];
    const formList = [
        {
            inputType: 'input',
            name: 'titleName',
            label: '标题名字',
        },
        {
            inputType: 'select',
            // multiple: 'multiple',//是否支持多选
            name: 'enterprise',
            label: '企业',
            options: [
                {
                    label: '企业一',
                    value: 0
                },
                {
                    label: '企业二',
                    value: 1
                },
                {
                    label: '企业三',
                    disabled: true,
                    value: 2
                },
            ]
        },
        {
            inputType: 'cascader',
            name: 'city',
            label: '城市',
            options: [
                {
                    value: '浙江',
                    label: '浙江',
                    children: [
                        {
                            value: '杭州',
                            label: '杭州',
                            children: [
                                {
                                    value: '西湖',
                                    label: '西湖',
                                },
                            ],
                        },
                    ],
                },
                {
                    value: '江苏',
                    label: '江苏',
                    children: [
                        {
                            value: '南江',
                            label: '南江',
                            children: [
                                {
                                    value: '中华门',
                                    label: '中华门',
                                },
                            ],
                        },
                    ],
                },
            ]
        },
        {
            inputType: 'timeSelect',
            name: 'time',
            label: '创建时间',
            format: 'YYYY-MM-DD HH:mm'
        }
    ]
    return (
        <>
            <Formsearch formList={formList} clickSearch={clickSearch}/>
            <YTable
                loading={tableLoading}
                data={data}
                columns={columns}
                pagination={pagination}
                selectionType={"checkbox"}
                defaultSelect={selectIds.value}
                selectionFunc={{getCheckbox, selectChange}}
                changeSize={changeSize}/>
        </>)
}
export default Table
