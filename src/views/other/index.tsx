import style from './index.module.scss'
import YModal from "@/components/YModal";
import {Button} from "antd";
import {useState} from "react";

const Other = () => {
    const [modal, setModal] = useState(false)
    const cancel = (type:string) => {
        console.log(type)
        setModal(false)
    }
    const confirm = () => {

    }
    return (<>
        <Button onClick={() => {
            setModal(true)
        }}>点击</Button>
        <YModal width="50%" title="标题" isModalOpen={modal} cancel={cancel} confirm={confirm}>666666</YModal>
    </>)

}
export default Other
