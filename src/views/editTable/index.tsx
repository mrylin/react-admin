import React, {useRef, useState} from "react";
import {useImmer} from "use-immer";
import {Form, Space} from "antd";
import TableButton from "@/components/YTable/component/tableButton";
import EditRow from "@/components/YTable/component/editRow";

const EditRowTable = () => {
    const [tableLoading, setTableLoading] = useState<boolean>(false)
    const [pagination, setPagination] = useImmer({
        pageSize: 10, //分页查询的条数
        current: 1, //当前页
        total: 200
    })
    const changeSize = ({pagination}: any) => {
        setEditingKey(null);
        setTableLoading(true)
        setPagination(draft => {
            draft.current = pagination.current
            draft.pageSize = pagination.pageSize
        })
        setTimeout(() => {
            setTableLoading(false)
        }, 800)
        console.log('分页', pagination)
    }

    const columns = [
        {
            editType: 'select',
            align: 'center',
            title: '排查描述',
            dataIndex: 'description',
            editable: true,
            ellipsis: true,
            options: [
                {
                    label: '描述一',
                    value: 0
                },
                {
                    label: '描述二',
                    value: 1
                },
            ],
            format: (e: any) => {
                if (e.description == 0) {
                    return '描述一'
                } else if (e.description == 1) {
                    return '描述二'
                } else {
                    return '未知'
                }
            },

        },
        {
            editType: 'select',
            align: 'center',
            title: '隐患级别',
            dataIndex: 'level',
            editable: true,
            ellipsis: true,
            format: (e: any) => {
                if (e.level == 0) {
                    return '选项一'
                } else if (e.level == 1) {
                    return '选项二'
                } else {
                    return '未知'
                }
            },
            options: [
                {
                    label: '选项一',
                    value: 0
                },
                {
                    label: '选项二',
                    value: 1
                },
            ]
        },
        {
            editType: 'input',
            align: 'center',
            title: '排查性质',
            dataIndex: 'nature',
            ellipsis: true,
            editable: true,
        },
        {
            editType: 'input',
            align: 'center',
            title: '流程状态',
            dataIndex: 'status',
            ellipsis: true,
            editable: true,
        },
        {
            editType: 'input',
            align: 'center',
            title: '排查人',
            dataIndex: 'people',
            ellipsis: true,
            editable: true,
        },
        {
            editType: 'input',
            align: 'center',
            title: '排查时间',
            dataIndex: 'time',
            ellipsis: true,
            editable: true,
        },
        {
            align: 'center',
            title: '操作',
            render: (text: any, record: any, index: any) => {
                return record.id === editingKey ? (
                    <Space>
                        <TableButton
                            click={() => editRowRef.current.saveClick(record)}>
                            保存</TableButton>
                        <TableButton
                            click={() => editRowRef.current.cancelClick()}
                            options={{type: 'dashed'}}>
                            取消</TableButton>
                    </Space>
                ) : (
                    <TableButton
                        click={() => editRowRef.current.editClick(record)}>
                        编辑</TableButton>

                )
            }
        },
    ]
    const [editingKey, setEditingKey] = useState<number | null>(null);
    const [form] = Form.useForm()
    const editRowRef = useRef<any>(null)
    const [data, setData] = useState([
        {
            id: 1,
            description: 0,
            level: 0,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 2,
            description: 1,
            level: 0,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 3,
            description: 1,
            level: 0,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 4,
            description: 0,
            level: 1,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 5,
            description: 0,
            level: 1,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 6,
            description: 1,
            level: 1,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 7,
            description: 0,
            level: 0,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 8,
            description: 0,
            level: 1,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 9,
            description: 1,
            level: 0,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 10,
            description: 1,
            level: 1,
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态10',
        },
    ])
    const saveRow = (row: any) => {
        console.log('保存成功', row)
    }

    return (
        <EditRow
            ref={editRowRef}
            form={form}
            pagination={pagination}
            changeSize={changeSize}
            editingKey={editingKey}
            data={data}
            functionGroups={{setData, setEditingKey, saveRow}}
            columns={columns}
            loading={tableLoading}
        />
    )
}
export default EditRowTable
