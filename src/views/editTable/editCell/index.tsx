import EditTheCell from "@/components/YTable/component/editTheCell";
import React, {useState} from "react";
import TableButton from "@/components/YTable/component/tableButton";
import {useImmer} from "use-immer";

const EditCell = () => {
    const [data, setData] = useState<any[]>([
        {
            id: 1,
            description: 0,
            level: '隐患级别1',
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 2,
            description: 1,
            level: '隐患级别2',
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 3,
            description: 1,
            level: '隐患级别3',
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 4,
            description: 0,
            level: '隐患级别4',
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 5,
            description: 0,
            level: '隐患级别5',
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 6,
            description: 1,
            level: '隐患级别6',
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 7,
            description: 0,
            level: '隐患级别7',
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 8,
            description: 0,
            level: '隐患级别8',
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 9,
            description: 1,
            level: '隐患级别9',
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态1',
        },
        {
            id: 10,
            description: 1,
            level: '隐患级别10',
            nature: '排查性质',
            people: '排查人22',
            time: '2022',
            status: '状态10',
        },
    ]);
    const defaultColumns = [
        {
            editType: 'select',
            title: '排查描述（可编辑）',
            dataIndex: 'description',
            align: 'center',
            ellipsis: true,
            editable: true,
            options: [
                {
                    label: '描述一',
                    value: 0
                },
                {
                    label: '描述二',
                    value: 1
                },
            ],
            format: (e: any) => {
                if (e.description == 0) {
                    return '描述一'
                } else if (e.description == 1) {
                    return '描述二'
                } else {
                    return '未知'
                }
            },
        },
        {
            title: '排查性质',
            dataIndex: 'nature',
            align: 'center',
            ellipsis: true,
        },
        {
            title: '流程状态',
            dataIndex: 'status',
            align: 'center',
            ellipsis: true,
        },
        {
            title: '隐患级别（可编辑）',
            editType: 'input',
            dataIndex: 'level',
            editable: true,
            align: 'center',
            ellipsis: true,

        },
        {
            title: '排查人',
            dataIndex: 'people',
            align: 'center',
            ellipsis: true,
        },
        {
            title: '排查时间',
            dataIndex: 'time',
            align: 'center',
            ellipsis: true,
        },
        {
            title: '操作',
            align: 'center',
            render: (text: any, record: any, index: number) => (
                <TableButton click={()=>delClick(record)}>删除</TableButton>
            )

        },
    ];
    const saveRow = (e: any) => {
        console.log('保存', e)
    }
    const delClick = (e: any) => {
        console.log('删除', e)
    }
    const [tableLoading, setTableLoading] = useState<boolean>(false)
    const [pagination, setPagination] = useImmer({
        pageSize: 10, //分页查询的条数
        current: 1, //当前页
        total: 200
    })
    const changeSize = ({pagination}: any) => {
        setTableLoading(true)
        setPagination(draft => {
            draft.current = pagination.current
            draft.pageSize = pagination.pageSize
        })
        setTimeout(() => {
            setTableLoading(false)
        }, 800)
        console.log('分页', pagination)
    }
    return (
        <EditTheCell
            loading={tableLoading}
            data={data}
            columns={defaultColumns}
            changeSize={changeSize}
            pagination={pagination}
            functionGroups={{setData, saveRow}}
        />
    )
}
export default EditCell
