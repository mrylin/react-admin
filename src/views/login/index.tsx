import SvgIcon from "@/components/svgIcon";
import style from "./index.module.scss"
import {Button, Checkbox, Form, Input} from "antd";
import {LockOutlined, UserOutlined} from "@ant-design/icons";
import {CheckboxChangeEvent} from "antd/es/checkbox";
import {useEffect, useState} from "react";
import {useImmer} from "use-immer";
import {setCookie} from "@/utils/cookieTool";
import {useLocation, useNavigate} from "react-router-dom";
import config from "@/config";
import {useDispatch} from "react-redux";
import { setUserName } from '@/store/modules/loginRelated'

interface accountVerify {
    selected: boolean
    value: string | null
}

export default () => {
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const user = localStorage.getItem('account');
    const [account, setAccount] = useImmer<accountVerify>({
        selected: !!user,
        value: '' || user
    })
    const [loginBtn, setLoginBtn] = useState<boolean>(false)
    const [form] = Form.useForm<any>()
    const onFinish = (values: any) => {
        //判断勾选记住账号
        if (account.selected) {
            localStorage.setItem('account', values.username)
        } else {
            localStorage.removeItem('account')
        }
        let userInfo = JSON.stringify({name: values.username, token: '1234567891002121'})
        setCookie('adminToken', userInfo)
        dispatch(setUserName(values.username))

        setLoginBtn(true) //禁用按钮
        setTimeout(() => {
            //关闭遮罩
            //loading.close();
            setLoginBtn(false) //放开禁用按钮
            // setMenu()
            //跳转到默认页面
            navigate('/', {replace: true})
        }, 1000)
    };
    const clickAccount = (e: CheckboxChangeEvent) => {
        setAccount(draft => {
            draft.selected = e.target.checked
        });
    };

    return (
        <div className={style.loginContainer}>
            <div>
                <SvgIcon iconName="company" svgClass={style.loginImg}/>
                <div className={style.loginForm}>
                    <div className={style.logoTitle}>
                        <SvgIcon iconName="logo" svgClass={style.logo}/>
                        <h3>React-Admin</h3>
                    </div>
                    <Form
                        form={form}
                        initialValues={{
                            username: account.value
                        }}
                        onFinish={onFinish}
                    >
                        <Form.Item
                            label=""
                            name="username"
                            rules={[{required: true, message: '请输入用户名'}]}
                        >
                            <Input size="large" prefix={<UserOutlined/>}
                                   placeholder="用户名：随便输"/>
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[{required: true, message: '请输入密码'}]}
                        >
                            <Input.Password size="large" prefix={<LockOutlined/>} placeholder="密码：随便输"/>
                        </Form.Item>
                        <Form.Item>
                            <Checkbox checked={account.selected} onChange={clickAccount}>记住账号</Checkbox>
                        </Form.Item>
                        <Form.Item>
                            <Button shape="round" loading={loginBtn} size={'large'} type="primary" htmlType="submit">
                                登录
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    )
}
