import {TableProps} from "antd";
// @ts-ignore
import {TableProps as RcTableProps} from "rc-table/lib/Table";

export interface props<T> extends TableProps<T> {
    data: Array<T>
    columns: Array<any> //props
    pagination?: interPagination//分页配置
    selectionType?: 'checkbox' | 'radio' //多选|单选
    selectionFunc?: { //勾选相关函数
        selectChange?: Function //选择框改变
        getCheckbox?: Function //判断禁用等
    }
    defaultSelect?: Array<T> //默认勾选
    changeSize?: Function //分页改变
    loading?: boolean //加载中
    editRow?: boolean //开启编辑行
    editColumns?: boolean //开启编辑列
    virtualTables?: any //虚拟表格
    scroll?: {  //table宽高
        x?: number
        y?: number
    } //可视区域
    showPagination?: boolean //是否显示分页
    showTooltip?: boolean //是否显示问号提示符
    customCell?: boolean //是否指定单元格自定义
}

export interface interPagination {
    pageSize?: number
    current?: number
    total?: number
    showTotal?: any
}
