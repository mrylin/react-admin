import React, {useEffect} from "react";
import {Form, Input, InputNumber, Select} from "antd";

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
    editing: boolean //是否在编辑中
    dataIndex: string //propsName
    inputType: string //编辑框类型
    record: any //row
    index: number //当前索引
    children: React.ReactNode //插槽
    columnsObj: any //props对象
}

const EditCell: React.FC<EditableCellProps> = ({
                                                   editing,
                                                   dataIndex,
                                                   inputType,
                                                   record,
                                                   index,
                                                   columnsObj,
                                                   children,
                                                   ...restProps
                                               }: EditableCellProps) => {
    let node: React.ReactNode, childrenNode: React.ReactNode = children
    if (inputType === 'number') {
        node =
            <InputNumber/>
    } else if (inputType === 'input') {
        node =
            <Input/>
    } else if (inputType === 'select') {
        node =
            <Select options={columnsObj?.options}/>
    }
    if (columnsObj?.format) {
        childrenNode = columnsObj?.format(record)
    }
    return (
        <td {...restProps}>
            {editing ? (
                <Form.Item
                    name={dataIndex}
                    style={{margin: 0}}
                >{node}</Form.Item>

            ) : childrenNode}
        </td>
    )
}
export default EditCell
