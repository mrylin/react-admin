import YTable from "@/components/YTable";
import {Form} from "antd";
import React, {forwardRef, useImperativeHandle} from "react";

interface props {
    loading?: boolean
    data: Array<any>
    columns: Array<any>
    pagination?: object
    changeSize?: Function
    form?: any
    editingKey?: string | null | number
    functionGroups?: {
        saveRow?: Function
        setData?: Function
        setEditingKey?: Function
    }
}

const EditRow = forwardRef(({
                                form,
                                data,
                                loading,
                                columns,
                                pagination,
                                changeSize,
                                editingKey,
                                functionGroups
                            }: props, editRowRef) => {
    useImperativeHandle(editRowRef, () => ({
        saveClick: save,
        cancelClick: cancel,
        editClick: edit
    }))
    const edit = (record: any) => {
        if (!(editingKey == null)) return
        form.setFieldsValue({...record});
        functionGroups?.setEditingKey?.(record.id)
    }
    const cancel = () => {
        functionGroups?.setEditingKey?.(null)
    };
    const save = async (record: any) => {
        try {
            //触发表单验证返回当前formItem中的对象
            const row = (await form.validateFields()) as any
            const {id} = record
            const newData = [...data]
            const index = newData.findIndex((item) => id == item.id)
            if (index >= 0) {
                const item = newData[index];
                newData.splice(index, 1, {
                    ...item,
                    ...row
                });
                functionGroups?.setData?.(newData)
                functionGroups?.setEditingKey?.(null)
                functionGroups?.saveRow?.({...record, ...row})
                console.warn("向后台保存成功的数据务必再向后台次查询一次数据，保持数据新状态")
            } else {
                newData.push(row)
                functionGroups?.setData?.(newData)
                functionGroups?.setEditingKey?.(null)
            }
        } catch (err: any) {

        }

    }
    const isEditing = (record: any) => record.id === editingKey

    const mergedColumns = columns.map(columnsObj => {
        if (!columnsObj.editable) {
            return columnsObj;
        }
        return {
            ...columnsObj,
            onCell: (record: any) => ({
                columnsObj,
                record,
                inputType: columnsObj.editType,
                dataIndex: columnsObj.dataIndex,
                editing: isEditing(record)
            }),
        };
    });
    return (
        <Form form={form} component={false}>
            <YTable
                editRow
                loading={loading}
                data={data}
                columns={mergedColumns}
                pagination={pagination}
                changeSize={changeSize}
            />
        </Form>
    )
})
export default EditRow
