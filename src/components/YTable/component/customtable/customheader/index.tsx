import React from "react";
import {Tooltip} from "antd";
import {QuestionCircleOutlined} from "@ant-design/icons";

interface props {
    tooltipTxt?: string //问号提示符
    children: any
}

const CustomHeader = ({tooltipTxt, children, ...propsData}: props) => {
    let childrenNode: React.ReactNode = children
    if (tooltipTxt) {
        childrenNode = <>{childrenNode}<Tooltip placement="top" title={tooltipTxt}>
            <QuestionCircleOutlined style={{marginLeft: 6, cursor: 'pointer'}}/>
        </Tooltip></>
    }
    return <th {...propsData}>
        {childrenNode}
    </th>
}
export default CustomHeader
