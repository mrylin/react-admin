import {props} from "@/components/YTable/interface";
import YTable from "@/components/YTable";

const CustomTable = ({
                        scroll,
                         showTooltip,
                         customCell,
                         loading,
                         data,
                         columns,
                         pagination,
                         selectionType,
                         defaultSelect,
                         selectionFunc,
                         changeSize
                     }: props<any>) => {

    const disposeColumns = columns.map((row: any) => {
        let obj
        if (showTooltip && customCell) {
            obj = {
                ...row,
                onHeaderCell: () => ({
                    row,
                    tooltipTxt: row?.tooltipTxt
                }),
                onCell: (celRow:any,index:number) => ({
                    celRow,
                    index,
                    customTxt: row?.customTxt,
                    clickNode: row?.clickNode
                }),
            }
        } else if (showTooltip){
            obj = {
                ...row,
                onHeaderCell: () => ({
                    row,
                    tooltipTxt: row?.tooltipTxt
                }),
            }
        } else if (customCell){
            obj = {
                ...row,
                onCell: (celRow:any,index:number) => ({
                    celRow,
                    index,
                    customTxt: row?.customTxt,
                    clickNode: row?.clickNode
                }),
            }
        }
        return obj
    });
    return <YTable
        scroll={scroll}
        showTooltip={showTooltip}
        customCell={customCell}
        loading={loading}
        data={data}
        columns={disposeColumns}
        pagination={pagination}
        selectionType={selectionType}
        defaultSelect={defaultSelect}
        selectionFunc={selectionFunc}
        changeSize={changeSize}
    />
}
export default CustomTable
