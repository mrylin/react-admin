import React from "react";
import {QuestionCircleOutlined} from "@ant-design/icons";
import {Tooltip} from "antd";

interface props {
    customTxt?: string
    clickNode?: Function
    celRow?: any
    index?: number
    children: any
}

const CustomBodyCell = ({celRow, customTxt, clickNode, index, children, ...propsData}: props) => {
    let childrenNode: React.ReactNode = children
    if (customTxt) {
        childrenNode = <>{childrenNode}<Tooltip placement="top" title={customTxt}>
            <QuestionCircleOutlined onClick={() => clickNode?.({celRow, index})}
                                    style={{marginLeft: 6, cursor: 'pointer'}}/>
        </Tooltip></>
    }
    return <td {...propsData}>
        {childrenNode}
    </td>
}
export default CustomBodyCell
