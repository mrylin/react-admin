import style from './index.module.scss'
import {Button, Space} from "antd";
import {ReactNode} from "react";
import {useImmer} from "use-immer";

interface props {
    options?: {
        danger?: boolean //是否是危险按钮
        disabledFunc?: Function
        ghost?: boolean //使按钮背景透明
        htmlType?: string //设置 button 原生的 type 值，可选值请参考 HTML 标准 https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button#attr-type
        icon?: ReactNode
        loading?: boolean
        shape?: 'default' | 'circle' | 'round' //设置按钮形状
        size?: 'large' | 'middle' | 'small' //设置按钮大小
        type?: 'primary' | 'ghost' | 'dashed' | 'link' | 'text' | 'default'
        className?: string,
        hide?: Function
    },
    children?: any,
    click?: Function
}

const TableButton = ({options, children, click}: props) => {
    return (
        <Button className={`table-button ${options?.className}`}
                style={options?.hide?.() ? {display: 'none'} : {}}
                danger={options?.danger}
                disabled={options?.disabledFunc?.()}
                ghost={options?.ghost}
                htmlType={options?.htmlType as any}
                icon={options?.icon}
                loading={options?.loading}
                shape={options?.shape}
                size={options?.size}
                type={options?.type || 'primary'} onClick={() => click?.()}>
            {children}
        </Button>
    )
}
export default TableButton
