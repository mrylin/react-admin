import React, {useState} from "react";
import TableButton from "@/components/YTable/component/tableButton";
import YTable from "@/components/YTable";
import {useImmer} from "use-immer";

interface props {
    loading?: boolean
    data: Array<any>
    columns: Array<any>
    pagination?: object
    changeSize?: Function
    functionGroups?: {
        setData?: Function
        saveRow?: Function
    }
}


const EditTheCell = ({
                         loading,
                         data,
                         columns,
                         pagination,
                         changeSize,
                         functionGroups
                     }: props) => {

    const handleSave = (row: any) => {
        const newData = [...data];
        const index = newData.findIndex((item) => row.id === item.id);
        const item = newData[index];
        newData.splice(index, 1, {
            ...item,
            ...row,
        });
        functionGroups?.setData?.(newData);
        functionGroups?.saveRow?.({...item, ...row})
        console.warn("向后台保存成功的数据务必再向后台次查询一次数据，保持数据新状态")
    };


    const defaultColumns = columns.map((row: any) => {
        if (!row.editable) {
            return row;
        }
        return {
            ...row,
            onCell: (record: any) => ({
                row,
                record,
                inputType: row?.editType,
                editable: row.editable,
                dataIndex: row.dataIndex,
                handleSave,
            }),
        };
    });
    return (<YTable
        editColumns
        loading={loading}
        data={data}
        columns={defaultColumns}
        pagination={pagination}
        changeSize={changeSize}/>)

}
export default EditTheCell
