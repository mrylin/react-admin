import React, {useContext, useEffect, useRef, useState} from "react";
import {Form, Input, InputNumber, Select} from "antd";
import {EditableContext} from '../context'
import style from './index.module.scss'

interface EditableCellProps {
    editable: boolean
    children: React.ReactNode
    dataIndex: any
    record: any
    row: any
    inputType: string
    handleSave: (record: any) => void
}


const EditableSingleCell: React.FC<EditableCellProps> = ({
                                                             editable,
                                                             children,
                                                             dataIndex,
                                                             record,
                                                             inputType,
                                                             handleSave,
                                                             row,
                                                             ...restProps
                                                         }: any) => {
    const [editing, setEditing] = useState(false);
    const inputRef = useRef<any>(null);
    const form = useContext<any>(EditableContext);

    useEffect(() => {
        if (editing) {
            inputRef.current!.focus();
        }
    }, [editing]);

    const toggleEdit = () => {
        setEditing(!editing);
        form.setFieldsValue({[dataIndex]: record[dataIndex]});
    };

    const save = async () => {
        try {
            const values = await form.validateFields();
            toggleEdit();
            handleSave({...record, ...values});
        } catch (errInfo) {

        }
    };
    let componentNode: React.ReactNode, childNode: React.ReactNode
    if (inputType === 'number') {
        componentNode =
            <InputNumber ref={inputRef} onPressEnter={save} onBlur={save}/>
    } else if (inputType === 'input') {
        componentNode =
            <Input ref={inputRef} onPressEnter={save} onBlur={save}/>
    } else if (inputType === 'select') {
        componentNode =
            <Select ref={inputRef} onBlur={() => setEditing(!editing)} onSelect={save}
                    options={row?.options}/>
    }
    if (row?.format) {
        childNode = row?.format(record)
    } else {
        childNode = children
    }

    if (editable) {
        childNode = editing ? (
            <Form.Item
                style={{margin: 0}}
                name={dataIndex}
            >{componentNode}</Form.Item>
        ) : (
            <div className={style.editableCellValueWrap} onClick={toggleEdit}>
                {childNode}
            </div>
        );
    }

    return <td {...restProps}>{childNode}</td>;
};
export default EditableSingleCell
