import style from './index.module.scss'
import {Table} from "antd";
import {useImmer} from "use-immer";
import PropTypes from "prop-types";
import {useEffect, useState} from "react";
import {compareArray} from "@/utils/tableCompareIds";
import {interPagination, props} from "@/components/YTable/interface";
import {TableRowSelection} from "antd/es/table/interface";
import EditableCell from "@/components/YTable/component/editRow/editCell";
import EditableSingleCell from "@/components/YTable/component/editTheCell/editablesinglecell";
import EditableRow from "@/components/YTable/component/editTheCell/editablerow";
import CustomHeader from "@/components/YTable/component/customtable/customheader";
import CustomBodyCell from "@/components/YTable/component/customtable/custombodycell";
import * as React from "react";

const YTable = ({
                    data,
                    columns,
                    pagination,
                    showPagination,
                    selectionType,
                    selectionFunc,
                    defaultSelect,
                    changeSize,
                    loading,
                    virtualTables,
                    editRow,
                    editColumns,
                    scroll,
                    showTooltip,
                    customCell
                }: props<any>) => {

    const [paginationConfig, setPaginationConfig] = useImmer<interPagination>({
        pageSize: 10, //分页查询的条数
        current: 1, //当前页
        total: 0,
        ...pagination,
        showTotal: (total: number): string => `总条数： ${total}`, //显示总条数
    })
    useEffect(() => {
        setPaginationConfig(draft => {
            draft!.current = pagination?.current
            draft!.pageSize = pagination?.pageSize
        })
    }, [pagination])
    const [tableData, setTableData] = useState<Array<any>>(data)
    useEffect(() => {
        setTableData(data)
    }, [data])

    const rowSelection: TableRowSelection<any> = {
        onChange: (selectedRowKeys: any, selectedRows: any) => {
            const change = compareArray(defaultSelect as any, selectedRowKeys)
            selectionFunc?.selectChange?.({selectedRowKeys, selectedRows, change})
        },
        getCheckboxProps: (record: any) => selectionFunc?.getCheckbox?.(record),
        defaultSelectedRowKeys: defaultSelect
    };
    const tableChange = (pagination: any) => changeSize?.({pagination})
    let components: any = false
    if (editRow) {
        components = {
            body: {
                cell: EditableCell
            }
        }
    } else if (editColumns) {
        components = {
            body: {
                row: EditableRow,
                cell: EditableSingleCell
            }
        }
    } else if (virtualTables) {
        components = {
            body: virtualTables
        }
    } else if (showTooltip || customCell) {
        if (showTooltip && customCell) {
            components = {
                body: {cell: CustomBodyCell},
                header: {cell: CustomHeader}
            }
        } else if (showTooltip) {
            components = {
                header: {cell: CustomHeader}
            }
        } else if (customCell) {
            components = {
                body: {cell: CustomBodyCell}
            }
        }
    }
    return (
        <div className="YTable">
            <Table scroll={scroll}
                   components={components}
                   loading={loading && {tip: "加载中..."}}
                   rowKey="id"
                   rowSelection={selectionType && {type: selectionType, ...rowSelection}}
                   columns={columns}
                   dataSource={tableData}
                   onChange={tableChange}
                   pagination={showPagination && paginationConfig || false}/>
        </div>)
}
YTable.propTypes = {
    data: PropTypes.array.isRequired,
    columns: PropTypes.array.isRequired,
    pagination: PropTypes.object,
    selectionType: PropTypes.string,
    selectionFunc: PropTypes.object,
    defaultSelect: PropTypes.array,
    changeSize: PropTypes.func,
    loading: PropTypes.bool,
    editRow: PropTypes.bool,
    showPagination: PropTypes.bool,
    showTooltip: PropTypes.bool,
    editColumns: PropTypes.bool,
    virtualTables: PropTypes.any,
    scroll: PropTypes.any,
};
YTable.defaultProps = {
    defaultSelect: [],
    showPagination: true,
    scroll: {
        y: 750
    }
};

export default YTable
