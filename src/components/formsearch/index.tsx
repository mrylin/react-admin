import PropTypes from "prop-types"
import style from './index.module.scss'
import {Button, Cascader, Form, Input, Select, DatePicker, ConfigProvider, Space} from "antd";
import {formatDate} from "@/utils/formatDate";

const {RangePicker} = DatePicker;


interface props {
    formList: Array<any>
    clickSearch?: Function
}

interface formObj {
    [name: string]: any // 一个 interface 中任意属性只能有一个
}

const FormSearch = ({formList, clickSearch}: props) => {
    const [form] = Form.useForm();
    const onSearch = async () => {
        const row = (await form.validateFields()) as any
        let obj: formObj = {}
        for (let key in row) {
            if (!!row[key] || row[key] == '0') {
                obj[key] = row[key]
                if (row[key]?.length === 2 && row[key][0] instanceof Object) {
                    obj[key] = row[key].map((item: any) => formatDate(item, 'YYYY-MM-DD HH:mm'))
                }
            }
        }
        clickSearch?.(obj)
    };
    const onReset = () => form.resetFields()
    return (
        <div className={style.formContainer}>
            <div className={style.leftForm}>
                <Form
                    layout="inline"
                    form={form}
                    name="basic"
                    autoComplete="off"
                >
                    <Space size={10}>
                        {
                            formList.map((item: any) => {
                                const {inputType} = item
                                switch (inputType) {
                                    case 'input':
                                        return (<Form.Item key={item.name}
                                                           label={item.label}
                                                           name={item.name}
                                        >
                                            <Input allowClear className={style.formWidth}
                                                   placeholder={`请输入${item.label}`}/>
                                        </Form.Item>);
                                    case 'select':
                                        return (<Form.Item key={item.name}
                                                           label={item.label}
                                                           name={item.name}
                                        >
                                            <Select className={style.formWidth}
                                                    mode={item?.multiple}
                                                    placeholder={`请选择${item.label}`}
                                                    options={item.options}/>
                                        </Form.Item>);
                                    case 'cascader':
                                        return (<Form.Item key={item.name}
                                                           label={item.label}
                                                           name={item.name}
                                        >
                                            <Cascader allowClear={false} className={style.formWidth}
                                                      options={item.options}
                                                      placeholder={`请选择${item.label}`}/>
                                        </Form.Item>);
                                    case 'timeSelect':
                                        return (<Form.Item key={item.name}
                                                           label={item.label}
                                                           name={item.name}
                                        >
                                            <RangePicker/>
                                        </Form.Item>);
                                }
                            })
                        }
                    </Space>
                </Form>
            </div>
            <div>
                <Space>
                    <Button type="primary" onClick={onSearch}>
                        搜索
                    </Button>
                    <Button onClick={onReset}>
                        重置
                    </Button>
                </Space>
            </div>
        </div>
    )
}
FormSearch.prototype = {
    formList: PropTypes.array,
    clickSearch: PropTypes.func
}
export default FormSearch
