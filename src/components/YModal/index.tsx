import {Button, Modal} from "antd";
import PropTypes from "prop-types";

interface props {
    isModalOpen?: boolean
    confirm?: Function
    cancel?: Function
    children?: any
    title?: string
    width?: string | number
}

const YModal = ({isModalOpen, confirm, cancel, children, title,width}: props) => {
    const handleOk = () => {
        console.log('确认')
        confirm?.()
    }
    const handleCancel = (type: 'cancel' | 'iconCancel') => {
        console.log('取消')
        cancel?.(type)
    }

    return (
        <Modal width={width} title={title} footer={
            [
                <Button key="confirm" type="primary" onClick={handleOk}>确认</Button>,
                <Button key="cancel" onClick={() => handleCancel('cancel')}>取消</Button>
            ]
        } open={isModalOpen} onCancel={() => handleCancel('iconCancel')}>
            {
                children
            }
        </Modal>
    )
}
YModal.propTypes = {
    isModalOpen: PropTypes.bool
}
YModal.defaultProps = {}
export default YModal
