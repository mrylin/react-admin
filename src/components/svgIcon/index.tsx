import {useEffect, useState} from "react";
import style from "./index.module.scss"
import PropTypes from "prop-types";

const SvgIcon = ({svgClass, iconName, svgStyle}: any) => {
    const [symbolId, setSymbolId] = useState<string>('')
    useEffect(() => {
        setSymbolId(`#icon-${iconName}`)
    }, [iconName])
    return (
        <svg className={`${style.iconSvg} ${svgClass}`} aria-hidden="true"
             style={svgStyle}>
            <use xlinkHref={symbolId}/>
        </svg>
    )
}
SvgIcon.propTypes = {
    svgClass: PropTypes.string,
    iconName: PropTypes.any.isRequired,
    svgStyle: PropTypes.object
};

SvgIcon.defaultProps = {
    svgClass: '',
    iconName: '',
    svgStyle: {}
};
export default SvgIcon
