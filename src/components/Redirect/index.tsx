import React from "react";
import {Navigate} from "react-router-dom";

interface Props {
    path: string
    replace?: boolean
}

const Redirect = (props: Props) => {
    const {path, replace} = props
    return <Navigate to={path} replace={replace}/>
}
export default Redirect
