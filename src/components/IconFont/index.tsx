import {createFromIconfontCN} from "@ant-design/icons";
import PropTypes from "prop-types";
import style from "./index.module.scss"

interface interProps {
    icon: string
    iconClass?: string
}

const IconFont = ({icon, iconClass}: interProps) => {
    const Icon = createFromIconfontCN({
        scriptUrl: './fonts/foundation/iconfont.js',
    });
    return <Icon type={icon || " "} className={iconClass}/>
}
IconFont.prototype = {
    icon: PropTypes.string,
    iconClass: PropTypes.string,
}
IconFont.defaultProps = {
    iconClass: style.iconfont
}
export default IconFont
