interface configVerify {
    devURL: string
    theme: object
}

export default {
    devURL: 'https://api.bilibili.com', //开发地址
    theme: {
        colorPrimary: '#AC5AFF' //主题色
    }
} as configVerify
