import React, {Suspense} from "react";
import {Spin} from "antd";

//路由懒加载
const lazyLoad = (Comp: React.LazyExoticComponent<any>): React.ReactNode => {
    return (
        <Suspense
            fallback={
                <Spin
                    spinning
                    size="large"
                    style={{
                        display: "flex",
                        flexDirection: "column",
                        marginTop: '10%',
                        alignItems: "center",
                        justifyContent: "center"
                    }}
                />
            }
        >
            <Comp/>
        </Suspense>
    );
};

export default lazyLoad;
