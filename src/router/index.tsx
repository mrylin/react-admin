import {createBrowserRouter, createHashRouter, Navigate, Outlet, redirect} from "react-router-dom";
import Layout from '@/layouts/index'
import App from "@/App";
import lazyLoad from "@/router/tool/lazyLoad";
import Redirect from '@/components/Redirect/index'
import React, {lazy} from "react";

export const router = createHashRouter([
    {
        element: <App/>,
        children: [
            {
                path: '/',
                element: <Redirect path='/layout/table' replace/>
            },
            {
                //访问layout重定向
                path: 'layout',
                element: <Redirect path='/layout/table' replace/>
            },
            {
                path: 'layout',
                element: <Layout/>,
                children: [
                    {
                        path: 'table',
                        element: lazyLoad(lazy(() => import('@/views/table/index'))),
                    },
                    {
                        path: 'editTable',
                        element: <Redirect path='/layout/editTable/editCell' replace/>
                    },
                    {
                        path: 'editTable',
                        children: [
                            {
                                path: "editCell",
                                element: lazyLoad(lazy(() => import('@/views/editTable/editCell/index'))),
                            },
                        ]
                    },
                    {
                        path: "editRowTable",
                        element: lazyLoad(lazy(() => import('@/views/editTable/index'))),
                    },
                    {
                        path: "virTable",
                        element: lazyLoad(lazy(() => import('@/views/virTable/index'))),
                    },
                    {
                        path: "customDemo",
                        element: lazyLoad(lazy(() => import('@/views/customdemo/index'))),
                    },
                    {
                        path: "other",
                        element: lazyLoad(lazy(() => import('@/views/other/index'))),
                    },
                ],
            },
            {
                path: "*",
                element: lazyLoad(lazy(() => import('@/views/error/E404/index'))),
            },
            {
                path: "login",
                element: lazyLoad(lazy(() => import('@/views/login/index'))),
            },
        ]
    },

]);
