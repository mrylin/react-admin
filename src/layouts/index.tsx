import style from "./index.module.scss"
import {Outlet} from "react-router-dom";
import {useState} from "react";
import Sider from "antd/es/layout/Sider";
import {Content, Header} from "antd/es/layout/layout";
import {Layout} from "antd";
import LayoutMenu from "@/layouts/menu";
import LayoutHeader from "@/layouts/header";
import LayoutLogo from "@/layouts/logo";

const Layouts = () => {
    const [collapsed, setCollapsed] = useState<boolean>(false);
    return (
        <Layout className={style.layoutContainer}>
            <Sider trigger={null} collapsible width={200}
                   className={style.leftSider}
                   collapsed={collapsed}>
                <LayoutLogo collapsed={collapsed}/>
                <LayoutMenu collapsed={collapsed}/>
            </Sider>
            <Layout>
                <Header>
                    <LayoutHeader collapsed={collapsed} setCollapsed={setCollapsed}/>
                </Header>
                <Content>
                    <div className={style.container}>
                        <Outlet/>
                    </div>
                </Content>
            </Layout>
        </Layout>
    )
}

export default Layouts
