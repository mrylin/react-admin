import menuArr from "@/router/tool/menu";
import {treeFindPath} from '@/utils/treeTool'

export const getMenuFatherID = () => menuArr.map((item: any) => item.id)
export const recursionID = (path: string, data: Array<any> = menuArr, res: Array<object> = []) => {
    data.forEach((item: any) => {
        if (path === item.url) {
            const openKeysArr = treeFindPath(menuArr, (treeData: any) => treeData.id === item.pid);
            res.push({id: item.id, openKeysArr})
        }
        if (item?.children.length) {
            recursionID(path, item.children, res)
        }
    })
    return res[0];
}
export const getMenuURL = (id: string, data: Array<any> = menuArr, res: Array<object> = []) => {
    data.forEach((item: any) => {
        if (id === item.id) {
            res.push({url: item.url})
        }
        if (item?.children.length) {
            getMenuURL(id, item.children, res)
        }
    })
    return res[0];
}

