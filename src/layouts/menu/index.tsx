import PropTypes from "prop-types";
import {Menu} from "antd";
import type {MenuProps} from 'antd';
import menuArr from '@/router/tool/menu'
import React, {useEffect, useState} from "react";
import IconFont from "@/components/IconFont";
import style from './index.module.scss'
import {useLocation, useNavigate} from "react-router-dom";
import {getMenuFatherID, getMenuURL, recursionID} from "@/layouts/menu/publicMethods";


interface interProps {
    collapsed: boolean
}

interface menuParam {
    id?: any
    openKeysArr?: any
    url?: any
}

const LayoutMenu = ({collapsed}: interProps) => {
    const location = useLocation()
    const navigate = useNavigate()

    type MenuItem = Required<MenuProps>['items'][number];
    const getItem = (
        label: React.ReactNode,
        key?: React.Key,
        icon?: React.ReactNode,
        children?: MenuItem[],
        type?: "group"
    ): MenuItem => {
        return {
            key,
            icon,
            children,
            label,
            type
        } as MenuItem;
    }
    const menuRecursion = (data: any) => data.map((item: any) => {
        if (item?.children.length) {
            return {
                ...getItem(item.name, item.id, <IconFont icon={item.icon} iconClass={style.menuIcon}/>,
                    menuRecursion(item.children))
            }
        } else {
            return {
                ...getItem(item.name, item.id, <IconFont icon={item.icon} iconClass={style.menuIcon}/>)
            }
        }
    })

    const rootSubmenuKeys: Array<any> = getMenuFatherID()

    useEffect(() => {
        const obj = recursionID(location.pathname) as menuParam
        if (!collapsed) {
            setTimeout(() => {
                setOpenKeys(obj?.openKeysArr)
                setCurrent(obj?.id)
            }, 200)
        } else {
            setOpenKeys(obj?.openKeysArr)
            setCurrent(obj?.id)
        }

    }, [location.pathname, collapsed])


    const [openKeys, setOpenKeys] = useState<Array<any>>([]);
    const onOpenChange = (keys: Array<string>) => {
        const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
        if (rootSubmenuKeys.indexOf(latestOpenKey!) === -1) {
            setOpenKeys(keys);
        } else {
            setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
        }
    }
    const [current, setCurrent] = useState<string>('');
    const onClick = ({key}: any) => {
        const {url} = getMenuURL(key) as menuParam
        navigate(url)
        setCurrent(key);
    }

    return (
        <div className="menu-container">
            <Menu
                onClick={onClick}
                openKeys={openKeys}
                onOpenChange={onOpenChange}
                selectedKeys={[current]}
                mode="inline"
                theme="dark"
                items={menuRecursion(menuArr)}
            />
        </div>
    )
};
LayoutMenu.propTypes = {
    collapsed: PropTypes.bool.isRequired,
};
export default LayoutMenu
