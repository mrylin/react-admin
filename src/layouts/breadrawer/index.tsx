import {Breadcrumb, Tabs} from "antd";
import {useEffect, useState} from "react";
import style from "./index.module.scss"
import menuArr from '@/router/tool/menu'
import {Navigate, NavLink, useLocation} from "react-router-dom";
import {treeFindPath} from "@/utils/treeTool";
import IconFont from "@/components/IconFont";

const BreadDrawer = () => {
    const location = useLocation()
    const [bread, setBread] = useState<Array<any>>()
    useEffect(() => {
        setBread(treeFindPath(menuArr, (data: any) => data.url === location.pathname, 'drawer'))
    }, [location.pathname])


    return (
        <Breadcrumb className={`bread-drawer ${style.container}`}>
            {
                bread?.map((item, index) =>
                    <Breadcrumb.Item key={index}>
                        {
                            item?.url ?
                                <NavLink to={item.url}>
                                    <IconFont icon={item.icon}/>
                                    {item.name}
                                </NavLink> :
                                <span> <IconFont icon={item.icon}/>{item.name}</span>
                        }
                    </Breadcrumb.Item>
                )
            }
        </Breadcrumb>)
}
export default BreadDrawer
