import {
    FullscreenExitOutlined,
    FullscreenOutlined, LogoutOutlined,
    MenuFoldOutlined,
    UserOutlined
} from "@ant-design/icons";
import style from "./index.module.scss"
import avatar from '@/assets/images/BiazfanxmamNRoxxVxka.png'
import {useRef, useState} from "react";
import PropTypes from "prop-types";
import {Avatar, Dropdown, Image, MenuProps, Popover, Space} from "antd";
import BreadDrawer from "@/layouts/breadrawer";
import {useLocation, useNavigate} from "react-router-dom";
import {delCookie} from "@/utils/cookieTool";
import {useSelector} from "react-redux";

interface interProps {
    collapsed: boolean
    setCollapsed: Function
}

const LayoutHeader = ({collapsed, setCollapsed}: interProps) => {
    const [angle, setAngle] = useState<number>(0)
    const [fullScreen, setFullScreen] = useState<boolean>(false)
    const userName = useSelector((state: any) => state.loginRelated.userName)
    const collapse = () => {
        if (collapsed) {
            setCollapsed(false);
            setAngle(0);
        } else {
            setCollapsed(true);
            setAngle(180);
        }
    }

    const fullScreenClick = () => {

        let element = document.documentElement as any;
        let cfs = document as any;
        if (fullScreen) {
            if (cfs.exitFullscreen) {
                cfs.exitFullscreen();
            } else if (cfs.webkitCancelFullScreen) {
                cfs.webkitCancelFullScreen();
            } else if (cfs.mozCancelFullScreen) {
                cfs.mozCancelFullScreen();
            } else if (cfs.msExitFullscreen) {
                cfs.msExitFullscreen();
            }
        } else {
            if (element.requestFullscreen) {
                element.requestFullscreen();
            } else if (element.webkitRequestFullScreen) {
                element.webkitRequestFullScreen();
            } else if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if (element.msRequestFullscreen) {
                // IE11
                element.msRequestFullscreen();
            }
        }
        setFullScreen(!fullScreen)
    }
    const navigate = useNavigate()
    const dropOut = () => {
        delCookie('adminToken')
        navigate('/login', {replace: true})
    }
    const items: MenuProps['items'] = [
        {
            key: '1',
            label: (<Space><UserOutlined/>个人中心</Space>),
        },
        {
            key: '2',
            label: (
                <span onClick={() => dropOut()}>
                    <Space><LogoutOutlined/>退出登录 </Space>
                </span>),
        },
    ];
    return (
        <div className={style.headerContainer}>
            <div>
                <Space size={16}>
                    <MenuFoldOutlined
                        onClick={collapse}
                        rotate={angle}
                        className={style.iconSize}
                    />
                    <BreadDrawer/>
                </Space>
            </div>
            <div className={style.rightAvatar}>
                <Space>

                    <Popover content={fullScreen ? '取消' : '全屏'} trigger="hover">
                        {
                            fullScreen ?
                                <FullscreenExitOutlined onClick={fullScreenClick} className={style.iconSize}/> :
                                <FullscreenOutlined onClick={fullScreenClick} className={style.iconSize}/>
                        }
                    </Popover>

                    <span>欢迎 {userName}</span>
                    <Dropdown menu={{items}} placement="bottom">
                        <Avatar size={36} src={<Image src={avatar} preview={false}/>}/>
                    </Dropdown>
                </Space>
            </div>
        </div>
    )
}
LayoutHeader.propTypes = {
    collapsed: PropTypes.bool.isRequired,
    setCollapsed: PropTypes.func.isRequired,
};

LayoutHeader.defaultProps = {
    collapsed: false,
};
export default LayoutHeader

