import SvgIcon from "@/components/svgIcon";
import style from "./index.module.scss"
import PropTypes from "prop-types";

interface interProps {
    collapsed: boolean
}

const LayoutLogo = ({collapsed}: interProps) => {
    return (
        <div className={style.logoContainer}>
            <SvgIcon iconName="logo" svgClass={style.logo}/>
            <h1 className={collapsed ? style.titleHide : style.titleShow}>ReactAdmin</h1>
        </div>
    )
}
LayoutLogo.prototype = {
    collapsed: PropTypes.bool.isRequired,
}
export default LayoutLogo
