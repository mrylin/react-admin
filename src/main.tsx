import React from 'react'
import {createRoot} from 'react-dom/client';
import 'virtual:svg-icons-register';
import 'antd/dist/reset.css';
import {RouterProvider} from "react-router-dom";
import config from "@/config";
import {Provider} from "react-redux";
import {router} from "@/router";
import {ConfigProvider} from "antd";
import store from "@/store/index";
import zh_CN from "antd/lib/locale/zh_CN";
import dayjs from 'dayjs';
import 'dayjs/locale/zh-cn';
dayjs.locale('zh-cn');
const container = document.getElementById('root') as any;
const root = createRoot(container);
root.render(
    <ConfigProvider locale={zh_CN}
        theme={{
            token: {
                ...config.theme
            },
        }}
    >
        <Provider store={store}>
            <RouterProvider router={router}/>
        </Provider>
    </ConfigProvider>
);
