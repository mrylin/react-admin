import {message} from 'antd';
import {ReactNode} from "react";


export const $success = (content: ReactNode | string) => {
    return message.success(content);
}
export const $error = (content: ReactNode | string) => {
    return message.error(content);
}
export const $warning = (content: ReactNode | string) => {
    return message.warning(content);
}
export const $info = (content: ReactNode | string) => {
    return message.info(content);
}
