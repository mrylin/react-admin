interface ResMap {
    add: Array<number | string>
    remove: Array<number | string>
}
interface CompareMap {
    [key: string]: string | number
}
/**
 * before => 差异前数组 type: Array<number | string>
 * after => 差异后数组 type: Array<number | string>
 */
export const compareArray = (before: Array<number | string>, after: Array<number | string>) => {
    let res: ResMap = {
        add: [],
        remove: []
    }
    let compareMap: CompareMap = {}
    for (let i = 0; i < before.length; i++) {
        compareMap[before[i]] = before[i]
    }
    // 差异比较 compareMap中保存的是before有的而after不存在的
    for (let i = 0; i < after.length; i++) {
        if (!compareMap[after[i]]) {
            res.add.push(after[i])
        } else {
            delete compareMap[after[i]]
        }
    }
    for (let j in compareMap) {
        res.remove.push(parseInt(j))
    }
    return res
}
