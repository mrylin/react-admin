import {isString} from './validate'
import dayjs from "dayjs";

// 格式化日期
export function formatDate(date: string | number | Date, format: string = 'YYYY-MM-DD HH:mm') {
    if (isString(date)) {
        // 时间戳格式化
        return dayjs(new Date(date)).format(format)
    } else {
        // 时间对象格式化
        return dayjs(date).format(format)
    }
}
