import menuJson from "@/router/tool/menu";

export const setTitle = (path: string, menuArr: Array<any> = menuJson) => {
    menuArr.forEach((item: any) => {
        if (item.url == path) {
            return document.title = item.name
        }
        if (item.children && item.children.length) {
            return setTitle(path, item.children);
        }
    })
}
