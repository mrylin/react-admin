/**
 * @param {string} str
 * @returns {Boolean}
 */
export function isString(str: any) {
    return typeof str === 'string' || str instanceof String;
}

/**
 * @param {Array} arg
 * @returns {Boolean}
 */
export function isArray(arg: any) {
    if (typeof Array.isArray === 'undefined') {
        return Object.prototype.toString.call(arg) === '[object Array]'
    }
    return Array.isArray(arg)
}

/**
 * @param {Array} arg
 * @returns {Boolean}
 */
export function isFunction(arg: any) {
    return Object.prototype.toString.call(arg) === '[object Function]'
}

export function isExternal(path: any) {
    return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str: any) {
    return str.trim().toString().length > 3
}

// 图片格式验证 jpg,png.word.ppt.excel.pdf

export function validImage(str: any) {
    const _t = str.toLowerCase()
    return /(.*)\.(jpg|gif|ico|jpeg|png)$/.test(_t)
}

export function validPdf(str: any) {
    const _s = str.toLowerCase()
    return /(.*)\.pdf$/.test(_s)
}

// 附件校验
export function validComment(str: any) {
    const _t = str.toLowerCase()
    return /(.*)\.(jpg|gif|ico|jpeg|png|doc|docx|pdf|xls|xlsx|excel)$/.test(_t)
}

// 电话验证
export function checkPhone(mobile: any) {
    const tel = /^0\d{2,3}-?\d{7,8}$/
    const phone = /^[1](([3|5|8][\d])|([4][4,5,6,7,8,9])|([6][2,5,6,7])|([7][^9])|([9][1,8,9]))[\d]{8}$/

    if (mobile.length == 11) {
        // 手机号码
        if (phone.test(mobile)) {
            return true
        }
    } else if (mobile.length == 13 && mobile.indexOf('-') != -1) {
        // 电话号码
        if (tel.test(mobile)) {
            return true
        }
    }
}

// 判断是否为整数
export function isInteger(num: any) {
    // 处理多0开头非法输入，列入 001  0001 000001
    if (typeof num === 'string') {
        if (num[0] === num[1] && num[0] === '0') return false
    }
    const reg = /^\+?[0-9][0-9]*$/
    return reg.test(num)
}

// 判断是否为数字
export function isNumber(oNum: any) {
    if (!oNum) return false

    const strP = /^\d+(\.\d+)?$/

    if (!strP.test(oNum)) return false

    try {
        if (parseFloat(oNum) != oNum) return false
    } catch (ex) {
        return false
    }

    return true
}

// 金额验证
export function validatePrice(price: any, maxPrice = 9999999999) {
    const reg = /(^[1-9](\d+)?(\.\d{1,2})?$)|(^[1-9]$)|(^\d\.[1-9]{1,2}$)|(^\d\.[0]{1}[1-9]{1}$|(^\d\.[1-9]{1}[0]{1}$)$)/
    let validate = true
    if (!reg.test(price) || price > maxPrice) {
        validate = false
    }
    return validate
}

// 验证 最多保留2位小数
export function validateDecimal(num: any) {
    const reg = /^(([0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/
    return reg.test(num)
}

// 验证密码 必须包含数字、大小写字母、特殊符号
export const validatePwd = (rule: any, value: any, callback: any) => {
    if (!value) {
        return callback(new Error('密码不能为空'))
    } else if (value.length < 10 || value.length > 20) {
        return callback(new Error('密码长度应在 10 到 20 个字符'))
    } else {
        const reg = /(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[^a-zA-Z0-9]).{10,20}/
        if (reg.test(value)) {
            callback()
        } else {
            return callback(new Error('密码必须包含大小写字母、数字、特殊字符4种'))
        }
    }
}
// 验证码 验证
export const validateCode = (rule: any, value: any, callback: any) => {
    if (!value) {
        return callback(new Error('验证码不能为空'))
    } else {
        if (value.length < 4) {
            return callback(new Error('验证码长度不少于4个字符'))
        } else {
            callback()
        }
    }
}

// 验证手机号
export const validateTel = (rule: any, value: any, callback: any) => {
    if (!value) {
        return callback(new Error('请输入手机号'))
    } else {
        const reg = /^(?:(?:\+|00)86)?1(?:(?:3[\d])|(?:4[5-79])|(?:5[0-35-9])|(?:6[5-7])|(?:7[0-8])|(?:8[\d])|(?:9[189]))\d{8}$/
        if (reg.test(value)) {
            callback()
        } else {
            return callback(new Error('手机号格式不正确'))
        }
    }
}

