import {defineConfig} from 'vite'
import path from "path";
import react from '@vitejs/plugin-react-swc'
import {createSvgIconsPlugin} from "vite-plugin-svg-icons";
import config from "./src/config";


export default defineConfig({
    resolve: {
        alias: [
            {
                find: '@',
                replacement: path.resolve(__dirname, 'src')
            },
            {
                find: '~',
                replacement: path.resolve(__dirname, 'node_modules')
            },
        ],
    },
    base: './',
    plugins: [
        react(),
        createSvgIconsPlugin({
            iconDirs: [path.resolve(process.cwd(), 'src/assets/svg')],
            symbolId: 'icon-[dir]-[name]'
        })],
    server: {
        host: '0.0.0.0',
        port: 4444,
        proxy: {
            // 使用 proxy 实例
            '/api': {
                target: config.devURL,
                changeOrigin: true,
                ws: true,
                rewrite: (path: any) => path.replace(/^\/api/, '')
            }
        },
    },
    css: {
        preprocessorOptions: {
            scss: {
                /*
                引入var.scss全局预定义变量，
                如果引入多个文件，
                可以使用
                '@import "@/assets/scss/globalVariable1.scss";@import "@/assets/scss/globalVariable2.scss";'
                这种格式
                 */
                additionalData: '@import "@/assets/css/public.scss";'
            }
        }
    }
})
